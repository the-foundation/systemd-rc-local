echo GENERATING rc.local and systemd job

cat >/etc/systemd/system/rc-local.service << EOF

[Unit]
 Description=/etc/rc.local Compatibility
 ConditionPathExists=/etc/rc.local

[Service]
 Type=forking
 ExecStart=/etc/rc.local start
 TimeoutSec=0
 StandardOutput=tty
 RemainAfterExit=yes
 SysVStartPriority=99

[Install]
 WantedBy=multi-user.target

EOF


test -f /etc/rc.local || ((echo '#!/bin/bash' ;echo '#insert your start scripts below - keep exit 0 as last line';echo ;echo "exit 0") > /etc/rc.local );
chmod +x /etc/rc.local


echo "ENABLING service "
systemctl enable rc-local

systemctl status rc-local.service
echo "check status with >>"systemctl status rc-local.service"<<"
echo  "you may now edit and test it with >>"systemctl start rc-local.service"<<"

