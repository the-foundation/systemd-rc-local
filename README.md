# systemd-rc-local

setup helper for enabling /etc/rc.local file in systemd environments 
normally /usr/lib/systemd/system-generators/systemd-rc-local-generator would help you 
but sometimes it is just not there

---

<a href="https://the-foundation.gitlab.io/">
<h3>A project of the foundation</h3>
<div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/systemd-rc-local/README.md/logo.jpg" width="480" height="270"/></div></a>
